package WebsiteTests;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.w3c.dom.Document;

import PagesRepository.Musala;
import StartWith.Processing;
import init.SeleniumWebTest;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import utils.FlushAPIresponse;
import utils.Randomgenerator;



//@Listeners(utils.TestMethodListener.class)
public class MusalaSoftTestCases extends SeleniumWebTest{
	
	
	Processing processing=new Processing();
	public long waitingTime=120;


	@Test(description="TestCase1")
	public void TestCase1() throws IOException
	{
		System.out.println(" TestCase1"+ " " +"Thread Id: " +Thread.currentThread().getId()); 
		Musala musalapage = new Musala(driver);
		Properties prop = processing.readPropertiesFile("TestData.properties");
		String[] emailList=processing.readInput("emailinput");
		String url = prop.getProperty("env");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver,waitingTime);
		for(int i=0;i<emailList.length;i++)
		{
	
	        try {
	    		driver.get(url);
	    		JavascriptExecutor js = (JavascriptExecutor) driver;
	    		// This  will scroll down the page by  1000 pixel vertical		
	    		js.executeScript("window.scrollBy(0,1000)");
				musalapage.contactUS.click();
				musalapage.personName.sendKeys("XYZ");
				musalapage.personEmail.sendKeys(emailList[i]);
				musalapage.personMobile.sendKeys("545556");
				musalapage.subject.sendKeys("Introduction");
				musalapage.personMessage.sendKeys("It's me XYZ");
				/*
				//int iframesize = driver.findElements(By.tagName("iframe")).size();
				//System.out.println("Size: "+iframesize);
				driver.switchTo().frame(1);
				try {
					Thread.sleep(15000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				musalapage.captcha.click();
				driver.switchTo().defaultContent();
				
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				*/
				musalapage.sendButton.click();
				//String verificationmessage=musalapage.verification.getText();
				//System.out.println("Verification: "+verificationmessage);
				wait.until(ExpectedConditions.visibilityOf(musalapage.validationEmail));
				String emailmessage=musalapage.validationEmail.getText();
				//System.out.println("EmailValidation: "+emailmessage);
				musalapage.popupClose.click();
				Assert.assertEquals("The e-mail address entered is invalid.", emailmessage);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}

	@Test(description="TestCase2")
	public void TestCase2() throws IOException
	{
		System.out.println(" TestCase2"+ " " +"Thread Id: " +Thread.currentThread().getId()); 
		Musala musalapage = new Musala(driver);
		Properties prop = processing.readPropertiesFile("TestData.properties");
		String url = prop.getProperty("env");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver,waitingTime);
		driver.get(url);
		wait.until(ExpectedConditions.visibilityOf(musalapage.company));
        //musalapage.company.click();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", musalapage.company);
        driver.manage().timeouts().implicitlyWait(waitingTime,TimeUnit.SECONDS);
        String companyurl=driver.getCurrentUrl();
        Assert.assertEquals(companyurl, url+"/company/");
        Assert.assertEquals(true, musalapage.leadership.isDisplayed());
        //musalapage.fblink.click();
        js.executeScript("arguments[0].click()", musalapage.fblink);
        driver.manage().timeouts().implicitlyWait(waitingTime,TimeUnit.SECONDS);
        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        String fburl=driver.getCurrentUrl();
        Assert.assertEquals(fburl, "https://www.facebook.com/MusalaSoft?fref=ts");
        //String imageText=processing.crackImage("./images/musala.jpg");
        //System.out.println("imageText: "+imageText);
        
        String logoSRC = musalapage.fblogo.getAttribute("src");
        URL imageURL = new URL(logoSRC);
        BufferedImage saveImage = ImageIO.read(imageURL);
        ImageIO.write(saveImage, "jpg", new File("./images/fblogo.jpg"));
        String downloadedimageText=processing.crackImage("./images/fblogo.jpg");
        downloadedimageText = downloadedimageText.replaceAll("\\s", "");
        //System.out.println("DownloadedimageText: "+downloadedimageText);
        
        Assert.assertEquals("MusalaSoft", downloadedimageText);
        
  	  
	}
	

	@Test(description="TestCase3")
	public void TestCase3() throws IOException
	{
		System.out.println(" TestCase3"+ " " +"Thread Id: " +Thread.currentThread().getId()); 
		Musala musalapage = new Musala(driver);
		Properties prop = processing.readPropertiesFile("TestData.properties");
		String url = prop.getProperty("env");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver,waitingTime);
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(waitingTime,TimeUnit.SECONDS);
		//wait.until(ExpectedConditions.visibilityOf(musalapage.careers));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", musalapage.careers);
        wait.until(ExpectedConditions.visibilityOf(musalapage.openPositions));
        musalapage.openPositions.click();
        String joinUSurl=driver.getCurrentUrl();
        String joinUSTitle=driver.getTitle();
        Assert.assertEquals("Join Us | Musala Soft", joinUSTitle);
        Assert.assertEquals(url+"/careers/join-us/", joinUSurl);

		Select drplocation = new Select(musalapage.locationList);
		drplocation.selectByVisibleText("Anywhere");
		musalapage.qaJob.click();
		String jobDetails=musalapage.qajobDetails.getText();
		if(jobDetails.contains("General description") && jobDetails.contains("Requirements") && jobDetails.contains("Responsibilities") && jobDetails.contains("What we offer"))
		{
		        js.executeScript("window.scrollBy(0,1000)");
				Assert.assertEquals(true, musalapage.applyButton.isDisplayed());
				wait.until(ExpectedConditions.visibilityOf(musalapage.applyButton));
				js.executeScript("arguments[0].click()", musalapage.applyButton);
				//musalapage.qaName.sendKeys("Test");
				musalapage.qaEmail.sendKeys("Test@test");
				musalapage.qaMobile.sendKeys("5531454");
				musalapage.qaCV.sendKeys("assignment.pdf");

		        js.executeScript("arguments[0].click()", musalapage.sendButton);
		        //driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		        wait.until(ExpectedConditions.visibilityOf(musalapage.errormsg));
		        String msg=musalapage.errormsg.getText();
		        //System.out.println("Error: "+msg);
		        Assert.assertEquals("One or more fields have an error. Please check and try again.", msg);
		        
		}
		
		
        
  	  
	}

	@Test(description="TestCase4")
	public void TestCase4() throws IOException
	{
		System.out.println(" TestCase4"+ " " +"Thread Id: " +Thread.currentThread().getId());
		Musala musalapage = new Musala(driver);
		Properties prop = processing.readPropertiesFile("TestData.properties");
		String url = prop.getProperty("env");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver,waitingTime);
		driver.get(url);
		wait.until(ExpectedConditions.visibilityOf(musalapage.careers));
        //musalapage.careers.click();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", musalapage.careers);
        wait.until(ExpectedConditions.visibilityOf(musalapage.openPositions));
        musalapage.openPositions.click();
        String joinUSurl=driver.getCurrentUrl();
        String joinUSTitle=driver.getTitle();
        Assert.assertEquals("Join Us | Musala Soft", joinUSTitle);
        Assert.assertEquals(url+"/careers/join-us/", joinUSurl);
        locationSelection(driver, "Sofia", musalapage);
        locationSelection(driver, "Skopje", musalapage);

		
	}
	
		

	@Test(description="TestCase6",enabled = false)
	public void TestCase6() throws IOException
	{
        File fileexpect = new File("./images/musala.jpg");
        File fileactual = new File("./images/musala2.jpg");

        BufferedImage bufferfileInput = ImageIO.read(fileexpect);
        DataBuffer datafileInput = bufferfileInput.getData().getDataBuffer();
        int sizefileInput = datafileInput.getSize();                     
        BufferedImage bufferfileOutPut = ImageIO.read(fileactual);
        DataBuffer datafileOutPut = bufferfileOutPut.getData().getDataBuffer();
        int sizefileOutPut = datafileOutPut.getSize();
        Boolean matchFlag = true;
        if(sizefileInput == sizefileOutPut) {                         
           for(int i=0; i<sizefileInput; i++) {
                 if(datafileInput.getElem(i) != datafileOutPut.getElem(i)) {
                       matchFlag = false;
                       break;
                 }
            }
        }
        else {                           
           matchFlag = false;
        Assert.assertTrue(matchFlag, "Images are not same");
        
		
        }
	}
	
	public void locationSelection(WebDriver driver,String location, Musala musalapage)
	{
		System.out.println("City: "+location);
		Select drplocation = new Select(musalapage.locationList);
		drplocation.selectByVisibleText(location);
		for(int i=1;i<=musalapage.jobList.size();i++)
		{
			String jobName=driver.findElement(By.xpath("//article["+i+"]/div/a/div/div[1]/h2")).getText();
			String jobLink=driver.findElement(By.xpath("//article["+i+"]/div/a")).getAttribute("href");
			System.out.println("Position"+i+": "+jobName);
			System.out.println("More info"+i+": "+jobLink);
		}
	}
	
}
