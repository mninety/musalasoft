package init;

import org.testng.annotations.BeforeMethod;

import StartWith.Processing;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Properties;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.DriverManagerType;
import io.github.bonigarcia.wdm.WebDriverManager;
import utils.FlushAPIresponse;

public class SeleniumWebTest {
	
	public WebDriver driver;

	
	

	@BeforeMethod
	public void setUp() throws IOException 
	{
		//TestData testdata = new TestData();
		Processing processing=new Processing();
		Properties prop = processing.readPropertiesFile("TestData.properties");
		String browserName = prop.getProperty("browserName");
		//System.out.println("Browser: "+browserName);

if(browserName.equalsIgnoreCase("chrome")) {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		//DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		//capabilities.setCapability("chrome.arguments", "-screenwidth 2048 -screenheight 1024");
		//driver.manage().window().maximize();
		driver.manage().window().setSize(new Dimension(512, 1024));
}
else if(browserName.equalsIgnoreCase("firefox")) {
		WebDriverManager.firefoxdriver().setup();
		driver = new FirefoxDriver();
}
else if(browserName.equalsIgnoreCase("ie")) {
		WebDriverManager.iedriver().setup();
		driver = new InternetExplorerDriver();
}
else if(browserName.equalsIgnoreCase("edge")) {
		WebDriverManager.edgedriver().setup();
		driver = new EdgeDriver();
}

else if(browserName.equalsIgnoreCase("headless")) {
		ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--headless");
		chromeOptions.addArguments("--disable-features=VizDisplayCompositor");
		chromeOptions.addArguments("--window-size=1920,1080");
		chromeOptions.addArguments("--start-maximized");
		chromeOptions.addArguments("--always-authorize-plugins");
		chromeOptions.addArguments("--allow-running-insecure-content");
		//chromeOptions.addArguments("--remote-debugging-port=9222");
		chromeOptions.addArguments("--whitelisted-ips"); 
		//chromeOptions.setBinary("/usr/bin/google-chrome");
		chromeOptions.addArguments("--disable-extensions"); // disabling extensions
		chromeOptions.addArguments("--disable-gpu"); // applicable to windows os only
		chromeOptions.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
		chromeOptions.addArguments("--no-sandbox"); // Bypass OS security model
		driver = new ChromeDriver(chromeOptions);

		
}
	}
	
	//Here we are quitting the driver
	@AfterMethod
	public void afterTest(ITestResult result) {

		   if (result.getStatus() == ITestResult.FAILURE) {
			   System.out.println(result.getMethod().getMethodName()+ " is failed");
		   }
		   else {
			   System.out.println(result.getMethod().getMethodName()+ " is passed");
		   }
		
		   driver.quit();
	}
}
