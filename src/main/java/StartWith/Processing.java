package StartWith;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class Processing {


	public void callMe2() throws IOException
	{

	      Properties prop = readPropertiesFile("TestData.properties");
	      String url=prop.getProperty("browserName");
	      System.out.println("URL: "+ url);

	}
	
	   public Properties readPropertiesFile(String fileName) throws IOException {
		      FileInputStream fis = null;
		      Properties prop = null;
		      try {
		         fis = new FileInputStream(fileName);
		         prop = new Properties();
		         prop.load(fis);
		      } catch(FileNotFoundException fnfe) {
		         fnfe.printStackTrace();
		      } catch(IOException ioe) {
		         ioe.printStackTrace();
		      } finally {
		         fis.close();
		      }
		      return prop;
		   }
	   public String[] readInput(String filename) throws IOException
	   {
		   BufferedReader br = new BufferedReader(new FileReader(filename));
		   String[] emailList;
		   try {
		       StringBuilder sb = new StringBuilder();
		       String line = br.readLine();

		       while (line != null) {
		           sb.append(line);
		           sb.append(System.lineSeparator());
		           line = br.readLine();
		       }
		       String everything = sb.toString();
		       //System.out.println("Test: "+everything);
		       emailList=everything.split("\n");
		   } finally {
		       br.close();
		   }
		   return emailList;
	   }
	   
	    public String crackImage(String filePath) throws IOException {
		      Properties prop = readPropertiesFile("TestData.properties");
		      String tessdata=prop.getProperty("tessdata");
	        ITesseract instance = new Tesseract();
	        try 
	        {
	        	instance.setDatapath("./images/tessdata");
	           String imgText = instance.doOCR(new File(filePath));
	           return imgText;
	        } 
	        catch (TesseractException e) 
	        {
	           e.getMessage();
	           return "Error while reading image";
	        }
	    }
	    
	    
}
