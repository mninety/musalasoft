package PagesRepository;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Musala {
	
	
	WebDriver driver;
	
	//Constructor
		public Musala(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}
	//Page constants
	@FindBy(xpath = "//a[@class='fancybox']/button")	
	public WebElement contactUS;
	
	@FindBy(name = "your-name")	
	public WebElement personName;
	
	@FindBy(name="your-email")
	public WebElement personEmail;
	
	@FindBy(xpath="//form/p[2]/span/span")
	public WebElement validationEmail;
	
	@FindBy(xpath="//a[@id='fancybox-close']")
	public WebElement popupClose;
	
	@FindBy(name="mobile-number")
	public WebElement personMobile;
	
	@FindBy(name="your-subject")
	public WebElement subject;
	
	@FindBy(name="your-message")
	public WebElement personMessage;
	
	@FindBy(xpath="//div[@class='recaptcha-checkbox-border']")
	public WebElement captcha;
	
	@FindBy(xpath="//div[@class='btn-cf-wrapper']/p/input")
	public WebElement sendButton;
	
	@FindBy(xpath="//div[@class='wpcf7-response-output']")
	public WebElement verification;
	
	@FindBy(xpath="//*[@id='menu-main-nav-1']/li[1]/a")
	public WebElement company;
	
	@FindBy(xpath="//div[@class='cm-content']/h2")
	public WebElement leadership;
	
	@FindBy(xpath="//div[@class='links-buttons']/a[4]")
	public WebElement fblink;
	
	@FindBy(xpath="//div[@class='_6tay']/img")
	public WebElement fblogo;
	
	@FindBy(xpath="//*[@id='menu-main-nav-1']/li[5]/a")
	public WebElement careers;
	
	@FindBy(xpath="//div[@class='link-wrapper']/a/button")
	public WebElement openPositions;
	
	@FindBy(name="get_location")
	public WebElement locationList;
	
	@FindBy(xpath="//h2[contains(text(),'Experienced Automation QA Engineer')]")
	public WebElement qaJob;
	
	@FindBy(xpath="//h2[contains(text(),'General description')]")
	public WebElement generaldes;
	
	@FindBy(xpath="//h2[contains(text(),'Requirements')]")
	public WebElement requirements;
	
	@FindBy(xpath="//h2[contains(text(),'Responsibilities')]")
	public WebElement responsibilities;
	
	@FindBy(xpath="//h2[contains(text(),'What we offer')]")
	public WebElement whatweoffer;
	
	@FindBy(xpath="//div[@class='entry-content']")
	public WebElement qajobDetails;
	
	@FindBy(xpath="//a[@class='fancybox']/input")
	public WebElement applyButton;
	
	@FindBy(xpath="//input[@name='your-name']")
	public WebElement qaName;
	
	@FindBy(xpath="//input[@name='your-email']")
	public WebElement qaEmail;
	
	@FindBy(xpath="//input[@name='mobile-number']")
	public WebElement qaMobile;
	
	@FindBy(xpath="//input[@name='uploadtextfield']")
	public WebElement qaCV;
	
	@FindBy(xpath="//span[@role='checkbox']")
	public WebElement recaptcha;
	
	@FindBy(xpath="//div[@class='wpcf7-response-output']")
	public WebElement errormsg;
	
	@FindBy(xpath="//article")
	public List<WebElement> jobList;

	
	public void getJobList(int i){

		jobList.get(i).click();
	}
}

